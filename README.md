# Dermatology
## Motivation:
General practicioners (GPs) see patients with melanocytic lesions and often have to distinguish among 'compound nevus', 'dermal nevus', 'seborrheic keratosis' and 'melanoma'. This diagnosis would be ideally done by a dermatologist with the aid of a dermatoscope. Aside from the limited time a GP can dedicate to a patients, GPs do not have access to a dermatoscope or are trained to use it. This project aims to use Mask R CNN to automatically discriminate the type of skin patologies so it can aid GPs on their diagnosis and improve the referal to the specialist.

![concept](images/concept.png)

## training and prediction

I started with four images of each of the melanocytic lesions. I manually labeled each image as belonging to one of the four types of lessions using the [labelme](https://github.com/wkentaro/labelme) tool. These are screenshots from the program, the json and jpeg files of the labeling step can be found [here](training/). I then used the annorated images to train the 'heads' of this [Mask R CNN](https://github.com/matterport/Mask_RCNN) model implementation.

![concept](images/labelme.png)

The **training script** that uses the Mask R CNN code implementation is [here](src/training/) and the [pre-trained coco weights](https://cocodataset.org/). The details of the traning paramaters such us the number of epochs and data augmentation can be easily found in the script.

    python dermatology/src/training/dermatology.py \
    dermatology/training \
    dermatology_model \
    mask_rcnn_coco.h5

The weight files produded by the traning step can be found here: [mask_rcnn_dermatology_cfg_0099.h5](https://drive.google.com/file/d/13p563l-VeUmuhwbMwHQr-Xn_MJeS00J_/view?usp=sharing)

The prediction script can be found [here](src/prediction/).
	
	python dermatology/src/prediction/predict.py \
	dermatology/training/images/dermal_nevus_00008.jpg \
	mask_rcnn_dermatology_cfg_0099.h5 \
	/OUTPUTDIR

**_As expected, sixteen images four each type of the melanocytic lesions are not enough to solve this task. Here I see an image of dermal nevus (green)
where also a false compound nevus (red) is detected. MORE DATA IS ON THE WAY!!_** 


![prediction](images/dermal_nevus_00008.annotated.png)

## Colab notebook:
The colab notebook I used for traning with GPU [Dermatology_Mask_RCNN.ipynb](https://colab.research.google.com/drive/1a_gkBBUAnqArlPa-vb2dlHs8NKX1_LRb?usp=sharing). The relevant versions there to point out are:
	
	tensorflow-gpu==1.13.1	
	keras==2.1.0

## Notes on versions for Tensorflow, Keras and Mask R CNN I that I use when running the predicion script locally:
	Tensorflow: 1.5.0
	Keras: 2.0.8
	Mask R CNN: https://github.com/matterport/Mask_RCNN/releases/tag/v2.1

	In file keras/models.py set:
		workers=1
		use_multiprocessing=False
	in:
		self.model.predict_generator(generator, steps,
		max_queue_size=max_queue_size,
		workers=1,
		use_multiprocessing=False,
		verbose=verbose)
	and:
		self.model.fit_generator(generator,
		steps_per_epoch,
		epochs,
		verbose=verbose,
		callbacks=callbacks,
		validation_data=validation_data,
		validation_steps=validation_steps,
		class_weight=class_weight,
		max_queue_size=max_queue_size,
		workers=1,
		use_multiprocessing=False,
		initial_epoch=initial_epoch)
	
	In file keras/engine/training.py, inside fit_generator function set:
		workers=1
		use_multiprocessing=False
